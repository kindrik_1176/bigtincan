

import { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './Profile.module.css';

const ProfileCard = (props) => {

    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios('https://jsonplaceholder.typicode.com/users');
       
            setData(result.data);
          };
       
          fetchData();
    }, []);

    console.log('state:', data);

    return (

        <div className={styles.flex_grid}>
            {data.map((item, index) => (
                <div className={styles.col} key={index}>
                    <div className={styles.left}>
                        <div className={styles.innerDiv}>
                            <div className={styles.circle}></div>
                        </div>
                    </div>
                    <div className={styles.right}>
                        <div className={styles.innerRight}>
                            <h2>{item.name}</h2>
                            <p>{item.email}</p>
                            <p>{item.address.street} {item.address.suite} {item.address.city} {item.address.zipcode}</p>
                            <p>{item.phone}</p>
                            <p><a href='javascript:;'>{item.website}</a></p>
                            <p>{item.company.catchPhrase}</p>
                        </div>
                    </div>
                </div>
            ))}
        </div>
     );
}
 
export default ProfileCard;
